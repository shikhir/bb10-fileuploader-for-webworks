<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

$target_dir = "uploads/";
if(strlen(basename($_FILES["fileToUpload"]["name"]))==0){
    exit('{ "success": false, "message":"file error - '.$_FILES["fileToUpload"]["name"].'"}');
}
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
    	exit('{ "success": false, "message":"file not an image"}');
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    exit('{ "success": false, "message":"file already exists"}');
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 50000000) {
    exit('{ "success": false, "message":"file too large."}');
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    exit('{ "success": false, "message":"Sorry, only JPG, JPEG, PNG & GIF files are allowed."}');
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    exit('{ "success": false, "message":"Sorry your file was not uploaded."}');
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    	$msg = '{ "success": true, "message": "File has been uploaded to '.$target_file.'"}';
    	exit($msg);
    } else {
    	exit('{ "success": false, "message":"error uploading your file."}');
    }
}
?>

