# I-Report for BlackBerry 10

This is a BB10 Cordova/WebWorks app lets users upload images or videos, along with their current GPS location from their BB10 device to a server. This app could be useful for field technicians who need to document or report their findings.

The sample code for this application is Open Source under the [Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0.html).


**Author** 

* [Shikhir Singh](http://www.shikhir.com/)


**Components** 

* BB10 App - A BlackBerry 10 WebWorks app 
* Server Code - A simple PHP code that read from form input the PIN, Latitude, Longitude, and file upload. Useful in testing the BlackBerry 10 code. 

**Dependencies to compile**

* [BlackBerry WebWorks v2.2](https://developer.blackberry.com/html5/download/) - Cordova may also work(untested)

**Wait, I just want to see how this app runs without compiling**

* A sample binary is provided in the bb10-app/bin directory
* A couple screenshot are also included


**Steps to compile the app**


1. (Optional) Replace application icon, file: icon.png
2. (Optional) Change application name located in www/config.xml under the <name> tag. Be very careful changing other fields. 
3. (Optional) Change the splash image located in www/img
4. Change the xml [access origin](https://developer.blackberry.com/html5/documentation/v2_2/access_element.html) tag in www/config.xml for your servers location.
5. Change the all desired configurations info in settings.js including the url for server 
6. Build the WebWorks app using the "webworks build" command from command prompt. See documentation for [Building and signing your completed app](http://developer.blackberry.com/html5/documentation/v2_2/build_and_sign_your_app.html). See example below. If no errors, your final binary is ready! 
7. Prior to deploying app on device, make sure the BlackBerry device is running version 10.1.0+. The app can be sideloaded for testing or deployed using BES10+. 


WebWorks Build Example (from step 6 above):
```
webworks build --release --keystorepass MyPasswordGoesHere --buildId 3
```

**POST variables/data sent to server**

* lat - Latitude of device
* lon - Longitude of device
* pin - PIN of device
* fileToUpload - File being uploaded to server

**Troubleshooting**

* Check the config.xml file again. Make sure your access origin tag is correctly set. Don't put the full url in this space, just the domain.
* Make sure you are not overwriting images on the server. Camera file names across devices are similar. It's the servers responsibility to save file using unique names
* The device is expecting a response from a POST in a particular format. See the server .php file. 
* Make sure the server allows [cross-site http requests](https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS). Make sure the Access-Control-Allow-Origin header is set on server. See server code for sample. 
* If you get an Invoke-Target-Key compile error, you will need to change the invoke target id IN ALL SOURCE CODE LOCATIONS it's used. BB10 sources files are typically in .js, .cpp, .hpp, .xml, and .json files.

Invoke-Target-Key Error - If you are on a OSX, you can run the following grep/sed commands to replace target id in all source code files automatically. Be sure to change the value CHANGE-ME below. 
``` 
grep --include \*.cpp --include \*.hpp --include \*.xml --include \*.json --include \*.js -rli 'community.iReport' * | xargs sed -i '' 's/community.iReport/com.CHANGE-ME.iReport/g'
```


## Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.