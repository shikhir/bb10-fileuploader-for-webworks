/*
 * Copyright 2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
var predefined = {
	debug: false
}

var app = {
	// Application Constructor
	initialize: function() {
		this.bindEvents();
	},
	// Bind Event Listeners
	//
	// Bind any events that are required on startup. Common events are:
	// 'load', 'deviceready', 'offline', and 'online'.
	bindEvents: function() {
		document.addEventListener('deviceready', this.onDeviceReady, false);
	},
	// deviceready Event Handler
	//
	// The scope of 'this' is the event. In order to call the 'receivedEvent'
	// function, we must explicity call 'app.receivedEvent(...);'
	onDeviceReady: function() {	
		
		try{
			document.getElementById("appVersion").innerHTML=blackberry.app.version;	
			document.getElementById("appTitle").innerHTML=blackberry.app.name;
			var mypin = blackberry.identity.uuid;
			mypin= mypin.slice(2);
			document.getElementById("mypinid").innerHTML = mypin;
			document.getElementById("pin").value = mypin;
			
			document.getElementById("appDescription").innerHTML = GLOBAL.appDescription;			
		}
		catch(e){
			document.getElementById("appTitle").innerHTML="iReport";
			$('#mypin').hide();
			$('#ver').hide();
		}
				
		navigator.geolocation.getCurrentPosition(function(position){
			$('#lat').val(position.coords.latitude);
			$('#lon').val(position.coords.longitude);
		}, function(error){
			$('#lat').val(0);
			$('#lon').val(0);
		});

		
	},
	resetApp: function(){
		$('#mainPanel').show();
		$('#progressPanel').hide();
		$('#nextBtn').show();
		$('#previousBtn').hide();
		$('#uploadFile').val("");
		$('#desc').val("");
		var fileInput = $('#fileToUpload');
		fileInput.replaceWith(fileInput.val('').clone(true));
		
		return;
	},	
	upload: function(){
	
	    	if($('#desc').val().trim().length==0){
				console.log('Enter a description');
				blackberry.ui.toast.show("Error: Enter Description");
				return;
	    	}
			if($('#fileToUpload').val().length==0){
				console.log('Please select a file first');
				blackberry.ui.toast.show("Error: Please select a file first");
				return;
				
			}
	    	else{
		    	GLOBAL.fn = $('#fileToUpload').val().split('\\').pop();
	    	}
			
			if($("#fileToUpload")[0].files[0].size > GLOBAL.cellularFileSizeLimit * 1000000){
				if(blackberry.connection.type == blackberry.connection.WIFI || 
					blackberry.connection.type == blackberry.connection.ETHERNET){
					console.log("wifi being used. No action required!");
			 	}
			 	else{
						if(!GLOBAL.allowLargeFiles){
							console.log(blackberry.connection.type);
						
							blackberry.ui.toast.show("Error: Use wifi for large file uploads");
							return;
						}
						blackberry.ui.toast.show(GLOBAL.cautionMsg);				 	
			 	}

			}			
			
			xhr = new XMLHttpRequest();
			$('#mainPanel').hide();
			$('#progressPanel').show();
			$('#nextBtn').hide();
			
			var progressBar = document.querySelector('progress');
			var progressBarLabel = document.getElementById('barLabel');;
			// Update progress bar
			xhr.upload.addEventListener("progress", function (e) {
			    if (e.lengthComputable) {
			    	
			    	var fileSizeMb = e.total/1000000;
			    	var fsStr = fileSizeMb > 1 ? fileSizeMb.toFixed(1) : fileSizeMb.toFixed(3);
			    
					$('#progressMsg').html("File Size: "+fsStr +" MB");
			    
			      progressBar.value = (e.loaded / e.total) * 100;
			      progressBarLabel.innerHTML = Math.round(progressBar.value) +"% Uploaded"; // Fallback for unsupported browsers.
			    }
			}, false);
						
			
			// File uploaded
			xhr.addEventListener("load", function (res) {
				$('#previousBtn').show();
			
				try{
					var jsonRes = JSON.parse(res.target.responseText);
					
					console.log(jsonRes);
					console.log(res);
					if(jsonRes.success == true){
							$('#barLabel').html("Upload completed!");
					
							var msgBody = "Upload of "+GLOBAL.fn+" is complete! ";
							if(jsonRes.message && jsonRes.message.length >0){
								msgBody += jsonRes.message;
								$('#serverMsg').html(jsonRes.message);
								
							}

						  new Notification(blackberry.app.name + ": File Uploaded", {body: msgBody});

						return;
					}
					else{
						$('#progressMsg').html("Upload Failed!");
						progressBarLabel.innerHTML = "ERROR:"+jsonRes.message;						
						progressBar.value = 0;
					}
					return;
				}
				catch(e){
						$('#barLabel').html("ERROR: Upload Failed");
				}
			}, false);

			xhr.addEventListener("error", function () {
				$('#barLabel').html("Error: Could not upload file! Recheck network connectivity");
				$('#previousBtn').show();
			}, false);

			xhr.addEventListener("abort", function () {
				$('#barLabel').html("Error: Could not upload file! Aborted!");
				$('#previousBtn').show();
			}, false);

			xhr.addEventListener("timeout", function () {
				$('#barLabel').html("Error: Could not upload file. Timeout!");
				$('#previousBtn').show();
			}, false);

			
			var form = document.getElementById('the-form');
			var formData = new FormData(form);
			var urlPost = encodeURI(GLOBAL.url);
			xhr.open("POST", urlPost, true);
			xhr.send(formData);
		
	}
};